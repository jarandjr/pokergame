package Cards;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class HandOfCardsTest {

    @Test
    @DisplayName("The test for getting the sum of a players cards")
    public void testSumOfCardsInHand() {
        final int SUM_OF_CARDS = 15;
        var list = makeHand('S');
        var hand = new HandOfCards(list);
        assertEquals(hand.getSumOfCards(), SUM_OF_CARDS);
    }

    @Test
    @DisplayName("The test to see if the method for checking if a player has a flush works")
    public void testToCheckToSeeIfAHandMakeUpAFlush() {
        var list = makeHand('S');
        var hand = new HandOfCards(list);
        assertTrue(hand.checkForFlush());

        list.add(new PlayingCard('D',1));
        hand = new HandOfCards(list);
        assertFalse(hand.checkForFlush());
    }

    @Test
    @DisplayName("The test to see if the method for checking if a player has the queen of spade works correctly")
    public void testToCheckToSeeIfAHandHasTheQueenOfSpade() {
        var list = makeHand('S');
        var hand = new HandOfCards(list);
        assertFalse(hand.checkForQuennOfSpade());

        list.add(new PlayingCard('S', 12));
        hand = new HandOfCards(list);
        assertTrue(hand.checkForQuennOfSpade());
    }

    @Test
    @DisplayName("The test to see if the method for checking if a player has any cards that are hearts works")
    public void testToCheckToSeeIfAHandHasHearts() {
        final String HAND_GOT_NO_HEARTS = "No Hearts";
        final String HAND_GOT_HEARTS = "H1 H2 H3 H4 H5";
        var list = makeHand('S');
        var hand = new HandOfCards(list);
        assertEquals(HAND_GOT_NO_HEARTS, hand.getHeartsFromHand());

        list = makeHand('H');
        hand = new HandOfCards(list);
        assertEquals(HAND_GOT_HEARTS, hand.getHeartsFromHand().trim());
    }

    /**
     * The private method for making a hand that can be tested with
     *
     * @param S is the suit of the card
     * @return the list of cards for the HandOfCards class
     */
    private ArrayList<PlayingCard> makeHand(char S) {
        var list = new ArrayList<PlayingCard>();
        for (int i = 0; i <5; i++) {
            list.add(new PlayingCard(S, i+1));
        }
        return list;
    }
}
