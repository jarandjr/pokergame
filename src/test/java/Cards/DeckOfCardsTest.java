package Cards;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DeckOfCardsTest {

    @Test
    @DisplayName("The test to see if a deck has less cards left after dealing a hand")
    public void testToSeeIfDeckOfCardsHasLessCardsInItAfterDealingHand() {
        final int NUMBER_OF_CARDS_IN_DECK = 52;
        DeckOfCards deck = new DeckOfCards();
        assertEquals(NUMBER_OF_CARDS_IN_DECK, deck.getSize());
        deck.dealHand(5);
        assertEquals(NUMBER_OF_CARDS_IN_DECK-5, deck.getSize());
    }

    @Test
    @DisplayName("The test to check that the resetDeck() method works")
    public void testToCheckThatADeckCanBeReseted() {
        final int NUMBER_OF_CARDS_IN_DECK = 52;
        DeckOfCards deck = new DeckOfCards();
        assertEquals(NUMBER_OF_CARDS_IN_DECK, deck.getSize());
        deck.dealHand(5);
        assertTrue(NUMBER_OF_CARDS_IN_DECK > deck.getSize());
        deck.resetDeck();
        assertEquals(NUMBER_OF_CARDS_IN_DECK, deck.getSize());
    }
}
