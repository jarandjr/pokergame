package App;

import Cards.DeckOfCards;
import Cards.HandOfCards;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.util.Objects;

/**
 * The controller class for the behavior of the app
 */
public class Controller {

    @FXML
    public Button btnDealHand;
    public Label tfFlush;
    public Label tfQofS;
    public Label tfHearths;
    public Label tfSum;
    public ImageView subImage;
    public ImageView mainImage;
    public ImageView cardDealt;
    public Label tfCards;
    public Label cardsLeft;

    DeckOfCards deck;
    HandOfCards hand;

    /**
     * The constructor where the deck is being initialized
     */
    public Controller() {
        this.deck = new DeckOfCards();
    }

    /**
     * Action being made when the deal hand button is being pressed.
     *
     * Animation and som text updates are being made
     */
    @FXML
    public void handleOnClickDealHand() {
        btnDealHand.setText("New hand?");
        hand = new HandOfCards(deck.dealHand(5));
        mainImage.setImage(null);
        cardsLeft.setText("The deck is empty");
        if (hand.getSize() >= 5) {
            mainImage.setImage(new Image(getClass().getResourceAsStream("/Images/playingCardsFacingDownP.jpg")));
            dealCardAnimation();
            cardsLeft.setText(deck.getSize() + " cards left");
        }
    }

    /**
     * Action being made when the reset button is being clicked
     *
     * Every textlabel is now empty
     * The card animation is reseted
     * And some images is being switch back
     */
    @FXML
    public void handleOnClickReset() {
        resetCardAnimation();
        tfFlush.setText("");
        tfSum.setText("");
        tfQofS.setText("");
        tfHearths.setText("");
        tfSum.setText("");
        tfCards.setText("");
        btnDealHand.setText("Deal hand");

        mainImage.setImage(null);
        subImage.setImage(new Image(getClass().getResourceAsStream("/Images/deckOfCardsP.jpg")));

        deck.resetDeck();
        cardsLeft.setText(deck.getSize() + " cards left");
    }

    /**
     * The actions being made when the check hand button is clicked
     *
     * the dealingcard animation is being reset
     * textboxes are updated with info about the cards in the hand
     */
    @FXML
    public void handleOnClickCheckHand() {
        if (deck.getSize() < 52) {
            resetCardAnimation();
            mainImage.setImage(new Image(getClass().getResourceAsStream("/Images/handOfCards.jpg")));
            tfFlush.setText(Boolean.toString(hand.checkForFlush()));
            tfQofS.setText(Boolean.toString(hand.checkForQuennOfSpade()));
            tfHearths.setText(hand.getHeartsFromHand());
            tfCards.setText(hand.toString());

            if (hand.getSize() < 5)
                tfSum.setText(Integer.toString(0));
            else
                tfSum.setText(Integer.toString(hand.getSumOfCards()));
        }
        else
            tfCards.setText("Your hand is empty");
    }

    /**
     * The private method for animating card being dealt
     */
    private void dealCardAnimation() {
        Image card = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/singlePlayingCardP2.jpg")));
        cardDealt.setImage(card);
        TranslateTransition transition = new TranslateTransition();
        transition.setDuration(Duration.seconds(0.5));
        transition.setCycleCount(5);
        transition.setToX(-300);
        transition.setToY(-50);
        transition.setNode(cardDealt);
        transition.play();
    }

    /**
     * The private method for reseting the card being dealt animation
     */
    private void resetCardAnimation() {
        cardDealt.setImage(null);
        TranslateTransition transition = new TranslateTransition();
        transition.setDuration(Duration.seconds(0.05));
        transition.setCycleCount(5);
        transition.setToX(250);
        transition.setToY(50);
        transition.setNode(cardDealt);
        transition.play();
    }
}
