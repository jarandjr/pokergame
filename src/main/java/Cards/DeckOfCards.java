package Cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Represents a deck of card consisting of 51 playing card.
 *
 * @author Jarand J. Romestrand
 * @version 2022-03-11
 */
public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private List<PlayingCard> deckOfCards;

    /**
     * The constructor for a deck of cards
     * The constructor makes the deck and then shuffles it
     */
    public DeckOfCards() {
        this.deckOfCards = makeDeck();
        shuffleDeck();
    }

    /**
     * The method for dealing a hand of cards.
     *
     * @param n is the number of cards being dealt
     * @return a random list of cards with length n
     */
    public List<PlayingCard> dealHand(int n) {
        List<PlayingCard> dealtHand = new ArrayList<>();
        Random randomCard = new Random();
        if (deckOfCards.size() < n)
            return dealtHand;
        for (int i = 0; i < n; i++) {
            int cardIndex = randomCard.nextInt(deckOfCards.size());
            var card = deckOfCards.get(cardIndex);
            dealtHand.add(card);
            deckOfCards.remove(card);
        }
        return dealtHand;
    }

    /**
     * The method for shuffling the deck
     */
    public void shuffleDeck() {
        Collections.shuffle(deckOfCards);
    }

    /**
     * The method for reseting the deck.
     * This method can be used if the game is over and all the cards are returned to the deck
     */
    public void resetDeck() {
        deckOfCards = makeDeck();
        shuffleDeck();
    }

    /**
     * The private method for making a deck consisting of 52 cards
     * @return the list of cards of size 52
     */
    private List<PlayingCard> makeDeck() {
        final int NUMBER_OF_SUITS = 4;
        final int NUMBER_OF_DIFFERENT_CARDS = 14;
        List<PlayingCard> list = new ArrayList<>();

        for (int i = 0; i < NUMBER_OF_SUITS; i++) {
            for (int j = 1; j < NUMBER_OF_DIFFERENT_CARDS ; j++) {
                list.add(new PlayingCard(suit[i],j));
            }
        }
        return list;
    }

    /**
     * A method to get the number of cards in the deck
     * @return the number of cards remaining in the deck
     */
    public int getSize() {
        return deckOfCards.size();
    }

    /**
     * Overrided toString method
     * @return the deck as a String
     */
    @Override
    public String toString() {
        return "Deck Of Cards: "+ deckOfCards;
    }
}
