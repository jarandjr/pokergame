package Cards;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a hand of cards consisting of playing cards.
 *
 * @author Jarand J. Romestrand
 * @version 2022-03-11
 */
public class HandOfCards {
    private List<PlayingCard> dealtHand;

    /**
     * The constructor for the hand of cards
     *
     * @param dealtHand is the list of cards thats dealt to the hand
     */
    public HandOfCards(List<PlayingCard> dealtHand) {
        this.dealtHand = new ArrayList<>();
        this.dealtHand.addAll(dealtHand);
    }

    /**
     * The meethod for checking if the players hand has a flush
     *
     * @return true or false depending on if the player has a flush or false if the hand has less than five cards
     */
    public boolean checkForFlush() {
        if (dealtHand.size() < 5)
            return false;
        return dealtHand.stream().map(PlayingCard::getSuit).collect(Collectors.toList()).stream().distinct().count() <= 1;
    }

    /**
     * The method for checking if the players hand has a queen of spade
     *
     * @return true or false depending on if the player has te card or not
     */
    public boolean checkForQuennOfSpade() {
        var queenOfSpade = new PlayingCard('S', 12);
        return dealtHand.stream().filter(c -> c.getFace() == queenOfSpade.getFace() && c.getSuit() == queenOfSpade.getSuit()).count() == 1;
    }

    /**
     * The method for getting the sum of the cards in the players hand
     *
     * @return The sum of the players hand as an integer
     */
    public int getSumOfCards() {
        return dealtHand.stream().map(PlayingCard::getFace).reduce((a,b) -> a + b).get();
    }

    /**
     * The method for getting all the Heart-cards from the players hand
     *
     * @return the list of cards as a String on the format: "H1 H2 H3" Or "No Hearts" if the player has no hearts
     */
    public String getHeartsFromHand() {
        var sb = new StringBuilder();
        if (dealtHand.stream().filter(c -> c.getSuit() == 'H').count() == 0)
            sb.append("No Hearts");
        dealtHand.stream().filter(c -> c.getSuit() == 'H').forEach(c -> sb.append(c.getAsString()).append(" "));
        return sb.toString();
    }

    /**
     * Override toString() method
     *
     * @return The players hand as a String
     */
    @Override
    public String toString() {
        return  "Hand: " +
                dealtHand;
    }

    /**
     * The method for getting the number of cards in the hand
     * @return the number of cards in the players hand
     */
    public int getSize() {
        return dealtHand.size();
    }
}
